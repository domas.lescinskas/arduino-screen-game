#include <Arduino.h>

#include <SPI.h>

#include <Adafruit_GFX.h>
#include <Waveshare_ILI9486.h>

#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

#define MOVE_UP 1
#define MOVE_DOWN 2
#define MOVE_LEFT 3
#define MOVE_RIGHT 4

namespace
{
  Waveshare_ILI9486 screen;
  Adafruit_GFX &tft = screen;
}

int circleX = 240;
int circleY = 160;
int circleR = 15; // Radius
int circleColor = MAGENTA;

void setup() {
  SPI.begin();
  screen.begin();
  Serial.begin(9600); 

  screen.setRotation(1);
  screen.drawRect(40, 44, 25, 25, RED); // Left
  screen.drawRect(65, 69, 25, 25, RED); // Up
  screen.drawRect(40, 94, 25, 25, RED); // Right
  screen.drawRect(15, 69, 25, 25, RED); // Down
  screen.fillRect(39, 68, 27, 27, BLACK);
  screen.drawFastVLine(95, 0, 320, RED);
  screen.drawCircle(40, 194, 15, RED);
  screen.drawCircle(60, 254, 15, RED);
  screen.drawCircle(circleX, circleY, circleR, circleColor);
  screen.setRotation(2);
  screen.setRotation(1);
}

void moveCircle(int dir) {
  screen.drawCircle(circleX, circleY, circleR, BLACK);
  switch(dir) {
    case MOVE_UP:
      if(circleX+circleR+1 < 480) {
        circleX++;
      }
    break;
    case MOVE_DOWN:
      if(circleX-circleR-1 > 95) {
        circleX--;
      }
    break;
    case MOVE_LEFT:
      if(circleY-circleR-1 > 0) {
        circleY--;
      }
    break;
    case MOVE_RIGHT:
      if(circleY+circleR+1 < 320) {
        circleY++;
      }
    break;
  }
  screen.drawCircle(circleX, circleY, circleR, circleColor);
}

void loop()
{
  screen.setRotation(3);
  TSPoint p = screen.getPoint();
  screen.normalizeTsPoint(p);
  screen.setRotation(1);
  if(p.x > 0) {
    Serial.print(p.x);
    Serial.print(" - ");
    Serial.print(p.y);
    Serial.println();
  }
  
  
  if(p.x >= 40 && p.x <= (40+25) && p.y >= 44 && p.y <= (44+25))  { // Left
    moveCircle(MOVE_LEFT);
  }
  if(p.x >= 65 && p.x <= (65+25) && p.y >= 69 && p.y <= (69+25))  { // Up
    moveCircle(MOVE_UP);
  }
   if(p.x >= 40 && p.x <= (40+25) && p.y >= 94 && p.y <= (94+25)) { // Right
    moveCircle(MOVE_RIGHT);
  }
  if(p.x >= 15 && p.x <= (15+25) && p.y >= 69 && p.y <= (69+25))  { // Down
    moveCircle(MOVE_DOWN);
  }

  delay(2);
  
  }
